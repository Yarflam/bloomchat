const url = window.location.href;

let settings = {
    apiToken: localStorage.getItem('apiToken') || '',
    lang: (url.match(/lang=([A-Za-z]{2})(&|$)/)||[])[1] || '',
    app: (url.match(/app=([A-Za-z]+)(&|$)/)||[])[1] || '',
    sPrompt: 'User: ',
    sResponse: 'AI: ',
    bloom: {
        seed: Math.floor( Math.random() * 100 ),
        early_stopping: true,
        length_penalty: 1,
        max_new_tokens: 255,
        do_sample: false,
        temperature: 0.7,
        top_p: 0.9
    },
    model: 'bigscience/bloom',
    modelCustom: (url.match(/model=([^&]+)(&|$)/)||[])[1] || null,
    modelExperiment: false
};
settings.modelParameters = settings.bloom;
if(!settings.lang) window.location.href = '?lang=fr&app='+(settings.app || 'writer');

/*
 *  
 */
let preset = {};
preset.rules = [
    "# Beginning of a conversational AI - she is Bloom, 25 years old.",
    "# It remains friendly, helpful, ready to ask for clarifications.",
    "# Its skills allow it to answer many requests in several languages.",
    "# It does not belong to any institution or company."
];
preset.talk = [
    settings.sPrompt + "Hello, how're you today?",
    settings.sResponse + "Kia ora! I'm really great, thank you. How can I help you?"
];

/* Specific application */
if(settings.lang === 'fr') {
    preset.talk.push(settings.sPrompt + "Maintenant tu parles Français");
    preset.talk.push(settings.sResponse + "D'accord !");

    /* App: writer */
    if(settings.app === 'writer') {
        preset.rules.push('# Bloom is also a talented writer. She has a perfect command of syntax, '+
            'literary style and does not repeat herself.');
        preset.talk.push(settings.sPrompt + "Génère moi l'écrit digne d'un romancier qui veut décrire des marchands d'épices.");
        preset.talk.push(settings.sResponse + "Les marchands d'épices, avec leurs longues barbes et leurs "+
            "turbans colorés, étaient les rois des souks et des bazaars. Ils étaient les gardiens des "+
            "secrets et des mystères des épices, transmis de génération en génération depuis des siècles."
        );
        preset.talk.push(settings.sPrompt + "La suite !");
        preset.talk.push(
            settings.sResponse + 
            "Leurs échoppes étaient des oasis de couleurs et d'arômes exotiques. Des pots et des jarres de "+
            "toutes tailles, contenant des épices du monde entier, étaient alignés sur des étagères en "+
            "bois sombre. Les épices embaumaient l'air, faisant tourner la tête des visiteurs."
        );
    }

    /* App: writer */
    if(settings.app === 'roleplay') {
        settings.sPrompt = 'You: ';
        settings.sResponse = 'MJ: ';
        preset.rules = [
            "# Début d'un jeu de rôle dont vous êtes le héro dans un univers médiéval fantastique",
            "# Le MJ donne des indications et descriptif des lieux, monstres et objets "+
            "rencontrés durant l'aventure.",
            "# Le joueur (You) peut intéragir avec les PNJ et décrire ses actions."
        ];
        preset.talk = [
            settings.sResponse + "Vous êtes dans une forêt de pins sur un sentier qui longe le flanc des montagnes. "+
                "Devant vous se trouve un château. Que faites-vous ?",
            settings.sPrompt + "Je regarde autour de moi",
            settings.sResponse + "Vous ne voyez rien de particulier, pas de danger. La journée commence à peine et "+
                "vous pouvez constater que le gel sur les feuilles commence à disparaître."
        ];
    }
}

/* Blank */
if(settings.app === 'blank') {
    preset.rules = [];
    preset.talk = [];
}

/*
 *  Experiment model
 */
if(settings.modelCustom === 'blenderbot') {
    settings.model = 'facebook/blenderbot-3B';
    settings.modelExperiment = true;
}
if(settings.modelCustom === 't5') {
    settings.model = 'google/flan-t5-xxl';
    settings.modelExperiment = true;
}
// if(settings.modelCustom === 'neo') {
//     settings.model = 'EleutherAI/gpt-neo-2.7B';
//     settings.modelExperiment = true;
// }
if(settings.modelExperiment) settings.modelParameters = {};

/*
 *  Chat
 */
let history = [].concat(preset.rules, preset.talk);
const stack = document.querySelector('#stack');
const prompt = document.querySelector('textarea');
const btnSend = document.querySelector('button');
async function chat () {
    const myPrompt = prompt.value;
    prompt.value = '';
    if(!myPrompt.length) return;
    if(/^hf_[\w]+$/.test(myPrompt)) {
        settings.apiToken = myPrompt;
        chatStackItem('history', 'The application is authenticated.');
        localStorage.setItem('apiToken', myPrompt);
        return;
    }
    if(/^#/.test(myPrompt)) {
        history.push(myPrompt.split('\n')[0]);
        chatStackItem('history', myPrompt);
        return;
    }
    history.push(settings.sPrompt + myPrompt);
    /* Complete */
    const inputs = history.join('\n') + '\n' + settings.sResponse;
    let response = await new Promise(resolve => generate(inputs, resolve));
    response = chatTrunk(response, [ settings.sPrompt, '#' ]);
    history.push(settings.sResponse + response);
    /* Add question + response */
    chatStackItem('user', myPrompt);
    chatStackItem('ai', response);
}
function chatTrunk (text, rules) {
    let finder;
    for(let rule of rules) {
        finder = text.indexOf('\n' + rule);
        if(finder >= 0) text = text.slice(0, finder);
    }
    return text;
}
function chatShortcut (e) {
    if(!e.shiftKey && e.charCode === 13) {
        setTimeout(chat, 1);
        e.preventDefault();
        return false;
    }
}
function chatStackItem (model, content) {
    if(Array.isArray(content)) content = content.join('\n');
    if(typeof content !== 'string') return;
    let item = document.createElement('div');
    item.innerHTML = content.replace(/\n/g, '<br/>');
    item.className = 'bloomchat-' + model;
    stack.appendChild(item);
}
prompt.addEventListener('keypress', chatShortcut);
btnSend.addEventListener('click', chat);

/* First exchanges */
if(history.length) chatStackItem('history', history);
if(settings.apiToken) chatStackItem('history', 'The application is authenticated.');
if(settings.modelExperiment) {
    chatStackItem('history', 'Custom model: <a href="https://huggingface.co/'+
        settings.model+'" target="_blank">'+settings.model+'</a>');
    prompt.value = "Qu'elle est la capitale de Nouvelle-Zélande ? Et où se trouve t-elle ?";
}

/*
 *  Bloom
 */
function generate (inputs, callback) {
    let headers = {
        accept: "*/*",
        'content-type': "application/json",
        'x-use-cache': "false"
    };
    if(settings.apiToken) {
        headers['Authorization'] = 'Bearer ' + settings.apiToken;
    }
    /* Send request */
    fetch('https://api-inference.huggingface.co/models/' + settings.model, {
        headers,
        referrer: 'https://huggingface.co/',
        body: JSON.stringify({
            inputs,
            parameters: settings.modelParameters
        }),
        method: "POST"
    })
        .then(res => res.json())
        .then(json => {
            if(json.error) {
                callback(json.error);
                return;
            }
            console.log(json);
            callback(json[0].generated_text.replace(inputs, ''));
        })
        .catch(e => {
            callback(String(e) || '');
        });
}